<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ProductType;
use App\Service\FileUploader;


class AddProductController extends Controller
{
    /**
     * @Route("/add/product", name="add_product")
     */
    public function index(Request $request, FileUploader $fileUploader)
    {

        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            dump("bloho");
            /**
             * Pour mettre sur la bdd les informations d'un formulaire
             * on fait le formulaire comme d'hab, mais ici, on utilise
             * le manager et le persist/flush
             */
            $product = $form->getData();

            $file = $product->getImage();
            $fileName = $fileUploader->upload($file);
            $product->setImage($fileName);

            $em = $this->getDoctrine()->getManager();

            $em->persist($product);

            $em->flush();

            return $this->redirectToRoute('product', ["id" => $product->getId()]);
        }
        return $this->render('add_product/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    
}
