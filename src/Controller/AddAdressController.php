<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Adress;
use App\Form\AddAdressType;

class AddAdressController extends Controller
{
    /**
     * @Route("/add/adress", name="add_adress")
     */
    public function index(Request $request)
    {

        $adress = new Adress();
   
        $form = $this->createForm(AddAdressType::class, $adress);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            dump("Ia Orana !");
            /**
             * Pour mettre sur la bdd les informations d'un formulaire
             * on fait le formulaire comme d'hab, mais ici, on utilise
             * le manager et le persist/flush
             */
            $adress = $form->getData();
          
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($adress);

            $em->flush();


        } 
        return $this->render('add_adress/index.html.twig', [
            'controller_name' => 'AddAdressController',
            'form' => $form->createView()
        ]);
    }
}

