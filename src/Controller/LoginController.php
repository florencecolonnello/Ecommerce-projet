<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Form\UserType;

class LoginController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    
    public function index(AuthenticationUtils $utils)
    {
        //On récupère les erreurs d'authentification s'il yen a
        $error = $utils->getLastAuthenticationError();
        
        //On récupère le dernier username utilisé pour une connexion
        $lastEmail = $utils->getLastUsername();

        return $this->render('login/index.html.twig', [
            'error' => $error,
            'lastEmail' => $lastEmail
        ]);
    }

    /**
     * @Route("/login/update/{id}", name="update_user")
     */

    // public function update(User $user, int $id, Request $request)
    // {
    //     $form = $this->createForm( UserType::class, $user);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
        
    //         $this->getDoctrine()->getManager()->flush();
    //         return $this->redirectToRoute('user', ["id" => $product->getId()]);
    //     }

    //     return $this->render('add_product/update.html.twig', array(
    //         'user' => $user,
    //         'form' => $form->createView(),
    //     ));

    //     return $this->redirectToRoute('login', ["id" => $user->getId()]);
    // }
}
