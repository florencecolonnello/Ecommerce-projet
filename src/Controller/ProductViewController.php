<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Product;
use App\Form\ProductType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\File;
use App\Service\FileUploader;



class ProductViewController extends Controller
{

    /**
     * @Route("/product/{id}", name="product")
     */

    public function index(int $id, ProductRepository $repo, Request $request)
    {

        $product = $repo->find($id);

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($form->getData());
            return $this->redirectToRoute("home");
        }

        return $this->render('product_view/index.html.twig', [

            "form" => $form->createView(),
            "product" => $product

        ]);
    }

    /**
     * @Route("/product/remove/{id}", name="remove_product")
     */
    public function remove(Product $product)
    {

        $em = $this->getDoctrine()->getManager();

        $em->remove($product);

        $em->flush();

        return $this->redirectToRoute("home", []);
    }


    /**
     * @Route("/product/update/{id}", name="update_product")
     */

    public function update(Product $product, int $id, Request $request, FileUploader $fileUploader)
    {
        $product->setImage(new File("uploads/images/".$product->getImage()));
        dump($product);
        $form = $this->createForm( ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $product->getImage();
            $fileName = $fileUploader->upload($file);
            $product->setImage($fileName);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('product', ["id" => $product->getId()]);
        }

        return $this->render('add_product/update.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
        ));

        return $this->redirectToRoute('product', ["id" => $product->getId()]);
    }

}

